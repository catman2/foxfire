import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.event.*;

public class TitlebarPanel extends JPanel {
	private int titlebarHeight;
	public TitlebarPanel(int h) {
		super();
		this.setOpaque(false);
		titlebarHeight = h;
		//this.setBackground(Color.CYAN);
		this.setMaximumSize(new Dimension(1, titlebarHeight));//note we override this method
		this.setMinimumSize(new Dimension(1, titlebarHeight));
	}

	@Override
	public void setMaximumSize(Dimension s) {
	    Dimension currentMaxSize = getMaximumSize();
	    super.setMaximumSize(new Dimension(currentMaxSize.width, s.height));
	}




}