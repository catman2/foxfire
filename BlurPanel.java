import java.awt.*;
import javax.swing.*;
import java.awt.image.*;

class BlurPanel extends JPanel {
	private int titlebarHeight;
	public BlurPanel(int h) {
		super(new FlowLayout(FlowLayout.CENTER,0,0));
		titlebarHeight = h;
		this.setMinimumSize(new Dimension(1, titlebarHeight));
	}

}