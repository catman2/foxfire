import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.LayerUI;
import java.awt.image.*;
import java.awt.event.*;


public class MainUIPanel extends JPanel {
	private BlurPanel blurPanel;
	private TitlebarPanel titlebarPanel;
	private JButton saveButton;
	private JLabel label;



	MainUIPanel(JFrame rootFrame) {
		super();

		int titlebarHeight = 30;
		LayoutManager overlayLayout = new OverlayLayout(this);
		this.setLayout(overlayLayout);
		blurPanel = new BlurPanel(titlebarHeight);
		titlebarPanel = new TitlebarPanel(titlebarHeight);



		saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener()
		{
		  public void actionPerformed(ActionEvent e)
		  {
		    JDialog dialog = new JDialog(rootFrame, "You clicked the button.", true);
		    dialog.setLocationRelativeTo(rootFrame);
		    dialog.setVisible(true);
		  }
		});
		titlebarPanel.add(saveButton);
		titlebarPanel.setAlignmentX(0);
		titlebarPanel.setAlignmentY(0);

		ImageIcon bgi = new ImageIcon("region.gif");
		label = new JLabel();
		label.setIcon(bgi);
		blurPanel.setBackground(Color.RED);
		blurPanel.add(label);

		LayerUI<JComponent> layerUI = new BlurTopLayerUI(titlebarHeight);
		JLayer<JComponent> blurPanelLayer = new JLayer<JComponent>(blurPanel, layerUI);

		blurPanelLayer.setAlignmentX(0);
		blurPanelLayer.setAlignmentY(0);

		this.add(titlebarPanel);
		this.add(blurPanelLayer);

		//make sure height is above titlebarHeight
		this.setPreferredSize(new Dimension(320, 240));


		ComponentMover cm = new ComponentMover(rootFrame, titlebarPanel);

	}
}

class BlurTopLayerUI extends LayerUI<JComponent> {
  private BufferedImage mOffscreenImage;
  private BufferedImageOp mOperation;
  private int pixelsFromTopToBlur;

  private StackBlurFilter blurFilter;

  public BlurTopLayerUI(int pixels) {
	pixelsFromTopToBlur = pixels;

    //mOperation = getBlurFilter(30);

    blurFilter = new StackBlurFilter(10, 3);


  }



  @Override
  public void paint(Graphics g, JComponent c) {
    int w = c.getWidth();
    int h = c.getHeight();

    if (w == 0 || h == 0) {
      return;
    }

    // Only create the offscreen image if the one we have
    // is the wrong size.
    if (mOffscreenImage == null ||
            mOffscreenImage.getWidth() != w ||
            mOffscreenImage.getHeight() != h) {
      mOffscreenImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    }

    Graphics2D ig2 = mOffscreenImage.createGraphics();
    ig2.setClip(g.getClip());
    super.paint(ig2, c);
    ig2.dispose();

    Graphics2D g2 = (Graphics2D)g;
    g2.drawImage(mOffscreenImage, null, 0, 0);
    g2.drawImage(mOffscreenImage.getSubimage(0, 0, w, pixelsFromTopToBlur), blurFilter, 0, 0);
  }
}

