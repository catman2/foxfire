import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.LayerUI;
//import MainUIPanel;

public class Foxfire extends JFrame {
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createGUI();
            }
        });
	}

	private static void createGUI() {
		//Create the window frame
		JFrame windowFrame = new Foxfire();
		windowFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//windowFrame.setSize(800, 600);
		windowFrame.setUndecorated(true); // Hide the titlebar & borders
		//create the main panel
		MainUIPanel mainPanel = new MainUIPanel(windowFrame);


		windowFrame.add(mainPanel);


		windowFrame.pack();
		windowFrame.setVisible(true);
	}

}